package au.id.deejay.webserver.api;

/**
 * @author David Jessup
 */
public enum HttpMethod {
	OPTIONS,
	GET,
	HEAD,
	POST,
	PUT,
	DELETE,
	TRACE,
	CONNECT
}

package au.id.deejay.webserver;

/**
 * @author David Jessup
 */
public class MessageConstants {

	private MessageConstants() {}

	public static final String CRLF = "\r\n";
}
